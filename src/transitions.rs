use futures::lock::Mutex;
use lazy_static::lazy_static;

use rand::seq::SliceRandom;
use rand::thread_rng;

use rand::Rng;
use teloxide::macros::teloxide;
use teloxide::payloads::SendMessage;
use teloxide::prelude::*;
use teloxide::types::ChatId;

use std::convert::TryInto;

// ´THREADs
use std::sync::{Arc, Condvar};
use std::{thread, time::Duration};

use super::states::*;

use tokio::runtime::Runtime;
use tokio::time::*;

use teloxide::types::ParseMode::Html;

pub struct GameLobby {
    pub id: i32,
    pub started: bool,
    pub player_chat_ids: Vec<i64>,
}

lazy_static! {
    static ref LOBBYS: Mutex<Vec<GameLobby>> = Mutex::new(vec![]);
    static ref BOTINSTANCE: teloxide::prelude::AutoSend<teloxide::Bot> =
        Bot::from_env().auto_send();
}

#[teloxide(subtransition)]
async fn start(
    state: StartState,
    cx: TransitionIn<AutoSend<Bot>>,
    ans: String,
) -> TransitionOut<Dialogue> {
    if ans.starts_with("/new") {
        println!("Received /new commando");
        if LOBBYS.lock().await.len() > 1000 {
            cx.answer(format!("No free lobby slots available. Try again later"))
                .await?;
            next(state)
        } else {
            // Create and join new lobby
            let mut valid_lobby_id = false;
            let mut lobby_id = 0;
            while !valid_lobby_id {
                lobby_id = rand::thread_rng().gen_range(0..1000);
                valid_lobby_id = !(LOBBYS.lock().await.iter().any(|lobby| lobby.id == lobby_id))
            }

            cx.answer(format!(
                "Registered for next game with new lobby ID {}. Now use /start or /leave",
                lobby_id
            ))
            .await?;
            println!("Created lobby with ID: {}", lobby_id);
            LOBBYS.lock().await.push(GameLobby {
                id: lobby_id,
                started: false,
                player_chat_ids: vec![cx.chat_id()],
            });
            next(RegisteredState { lobby_id })
        }
    } else {
        if let Ok(lobby_id) = ans.parse() {
            println!("Received join lobby {} commando", lobby_id);
            // Join exiting lobby
            let mut found_lobby = false;
            let mut new_lobby_size = 0;
            for lobby in LOBBYS.lock().await.iter_mut() {
                match lobby {
                    GameLobby {
                        id,
                        started,
                        player_chat_ids,
                    } if (*id == lobby_id && *started == false) => {
                        println!("Joined lobby with ID: {}", id);
                        player_chat_ids.push(cx.chat_id());
                        new_lobby_size = player_chat_ids.len();
                        found_lobby = true;
                        break;
                    }
                    _ => (),
                }
            }
            if found_lobby {
                notfiy_lobby(
                    lobby_id,
                    format!(
                        "Player joined your lobby. Registered players: {}",
                        new_lobby_size
                    ),
                    cx.chat_id(),
                )
                .await;
                cx.answer(format!(
                    "Registered for next game in lobby {}. Now use /start, or /leave",
                    lobby_id
                ))
                .await?;
                // Transition
                next(RegisteredState { lobby_id })
            } else {
                cx.answer(format!("The lobby with ID {} is not existing or already started. Try to use another lobby ID or use /new to create a new lobby", lobby_id)).await?;
                // Transition
                next(state)
            }
        } else {
            println!("Received invalid commando from unregistered user: {}", ans);
            cx.answer("Please, send me a lobby ID or use /new to create a new lobby")
                .await?;
            next(state)
        }
    }
}

async fn notify_user(chat_id: i64, msg: String, notifcation: bool) {
    println!("Notfiy user {} with msg {}", chat_id, msg);
    BOTINSTANCE
        .send_message(chat_id, msg)
        .disable_web_page_preview(true)
        .disable_notification(!notifcation)
        .await
        .map(drop)
        .unwrap_or_else(|err| log::error!("Failed to notify users {}!", err));
}

async fn notfiy_lobby(lobby_id: i32, msg: String, exclude_user: i64) {
    for lobby in LOBBYS.lock().await.iter() {
        match lobby {
            GameLobby {
                id,
                started: _,
                player_chat_ids,
            } if *id == lobby_id => {
                println!("Notify all users of lobby: {}", lobby_id);
                for player_chat_id in player_chat_ids.iter() {
                    if *player_chat_id != exclude_user {
                        notify_user(*player_chat_id, msg.clone(), false).await;
                    }
                }
                break;
            }
            _ => (),
        }
    }
}

fn player_number_valid(player_number: i32) -> bool {
    if player_number >= 5 && player_number % 2 == 1 && player_number <= 11 {
        true
    } else {
        false
    }
}

async fn notfiy_roles(lobby_id: i32) {
    for lobby in LOBBYS.lock().await.iter() {
        match lobby {
            GameLobby {
                id,
                started: _,
                player_chat_ids,
            } if *id == lobby_id => {
                // Shuffel list
                let mut player_list = player_chat_ids.clone();
                player_list.shuffle(&mut thread_rng());

                // define teams
                let roles = vec![
                    // Red
                    "Team Red (You play open)".to_string(),
                    "Team Red (You play hidden)".to_string(),
                    // Blue
                    "Team Blue (You play open)".to_string(),
                    "Team Blue (You play hidden)".to_string(),
                    // Green
                    "Team Green (You play open)".to_string(),
                    "Team Green (You play hidden)".to_string(),
                    // White
                    "Team White (You play open)".to_string(),
                    "Team White (You play hidden)".to_string(),
                    // Black
                    "Team Black (You play open)".to_string(),
                    "Team Black (You play hidden)".to_string(),
                ];

                for (i, chat_id) in player_list.iter().enumerate() {
                    if i == player_list.len() - 1 {
                        notify_user(
                            *chat_id,
                            format!("Started game: Your role is: {}", "Octopus - Grrrrrr"),
                            true,
                        )
                        .await;
                    } else {
                        notify_user(
                            *chat_id,
                            format!("Started game: Your role is: {}", roles[i]),
                            true,
                        )
                        .await;
                    }
                }
            }
            _ => (),
        }
    }
}

#[teloxide(subtransition)]
async fn registered(
    state: RegisteredState,
    cx: TransitionIn<AutoSend<Bot>>,
    ans: String,
) -> TransitionOut<Dialogue> {
    let lobby_id = state.lobby_id;

    if ans.starts_with("/leave") {
        println!("Received /leave commando");

        let mut remaining_players = 0;

        for lobby in LOBBYS.lock().await.iter_mut() {
            match lobby {
                GameLobby {
                    id,
                    started: _,
                    player_chat_ids,
                } if *id == lobby_id => {
                    player_chat_ids.retain(|&id| id != cx.chat_id());
                    remaining_players = player_chat_ids.len();
                    println!("User {} left lobby with ID: {}", cx.chat_id(), id);
                }
                _ => (),
            }
        }
        if remaining_players > 0 {
            notfiy_lobby(
                lobby_id,
                format!(
                    "Player left lobby. Remaining players: {}",
                    remaining_players
                ),
                0,
            )
            .await;
        }

        LOBBYS
            .lock()
            .await
            .retain(|lobby| (*lobby).player_chat_ids.is_empty() == false);
        for lobby in LOBBYS.lock().await.iter() {
            println!("Lobbys open: {}", lobby.id)
        }

        cx.answer(format!("You have left lobby {}", lobby_id))
            .await?;
        next(StartState)
    } else if ans.starts_with("/start") {
        println!("Received /start commando");
        println!("Starting lobby with ID: {}", lobby_id);
        let mut start_valid = false;
        let mut player_count = 0;
        for lobby in LOBBYS.lock().await.iter_mut() {
            match lobby {
                GameLobby {
                    id,
                    started,
                    player_chat_ids,
                } if *id == lobby_id => {
                    if *started == false {
                        player_count = player_chat_ids.len();
                        if player_number_valid(player_count.try_into().unwrap()) {
                            println!("Started lobby with ID: {}", lobby_id);
                            *started = true;
                            start_valid = true;
                        } else {
                            cx.answer("Invalid player number. Could not start game.")
                                .await?;
                        }
                    } else {
                        cx.answer("Game already started.").await?;
                    }
                    break;
                }
                _ => (),
            }
        }
        if start_valid {
            notfiy_lobby(
                lobby_id,
                format!("Lobby {} started with {} players.", lobby_id, player_count),
                0,
            )
            .await;
            notfiy_roles(lobby_id).await;
        } else {
            println!("Failed to start lobby with ID: {}", lobby_id);
        }
        next(state)
    } else {
        println!("Received invalid commando {}", ans);
        cx.answer("Please, send /start or /leave").await?;
        next(state)
    }
}
