use crate::dialogue::{states::active::ActiveState, Dialogue};
use teloxide::prelude::*;


#[derive(Clone, Generic)]
pub struct RegisteredState;

#[teloxide(subtransition)]
async fn registered(
    state: RegisteredState,
    cx: TransitionIn<AutoSend<Bot>>,
    ans: String,
) -> TransitionOut<Dialogue> {
    cx.answer("You are registered for the next round").await?;
    next(ActiveState)
}

