use crate::dialogue::Dialogue;
use teloxide::prelude::*;

#[derive(Clone, Generic)]
pub struct ActiveState;

#[teloxide(subtransition)]
async fn active(
    state: ActiveState,
    cx: TransitionIn<AutoSend<Bot>>,
    ans: String,
) -> TransitionOut<Dialogue> {
    cx.answer(format!("Full name: {}",ans))
        .await?;
    exit()
}